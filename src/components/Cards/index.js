import React from 'react';
import Card from '../Card';
import './cards.css';
import PropTypes from 'prop-types';

export default function Cards({cards, favoriteCards, clickOpenModal, addToFavorites, modalId, context}) {
    
    return(
        <div className='cards_wrapper'>{cards.map((elem) => {
            return(
                <Card 
                    card={elem} 
                    key={elem.article}
                    favorite = {favoriteCards.map(c => c.article).includes(elem.article)}
                    clickOpenModal = {clickOpenModal}
                    addToFavorites = {addToFavorites}
                    modalId = {modalId}
                    context= {context}
                />
            )})} 
        </div>
    )
}

Cards.propTypes = {
    favoriteCards: PropTypes.array,
    clickOpenModal: PropTypes.func,
    addToFavorites: PropTypes.func,
}
