import React from 'react';
import basketIcon from '../../image/basketIcon.png';
import './basket.css';
import PropTypes from 'prop-types';

export default function Basket({basket}) {

    return(
        <div className='basket_wrapper'>
            <a className='basket-icon'>
                <img src={basketIcon}></img>
            </a>
            <div className='basket-count'>
                {basket.length}
            </div>
        </div>
    )
}


Basket.propTypes = {
    basket: PropTypes.array,
};


