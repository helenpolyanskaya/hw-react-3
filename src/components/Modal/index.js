import React from 'react';
import './modal.css';
import PropTypes from 'prop-types';
// import { useEffect, useState } from 'react';

export default function Modal({onClose, open, onAction1, onAction2, actions1, actions2, header, text}) {

    // constructor(props) {
    //     super(props);

    //     this.onClose = this.onClose.bind(this);
    // }
    
    // const onClose = () => {
    //     onClose()
    // }

    const showClass = () => {
        if(open === false){
            return 'off';
        }   
        return "";   
    }

    // render() {

        if(open === false){
            return null;
        }

        return (
         <div  id='modal_wrapper' onClick={(e) => e.target === e.currentTarget && onClose()}>
            <div className={`modal ${showClass()}`}>
                <div className='header_wrapper'>
                    <h2>{header}</h2>
                    <button className='toggle-button' onClick={onClose}>
                        X
                    </button>
                </div>
                <div className='content'>
                    <h3>{text}</h3>
                    <button onClick = {onAction1} style={{border: '1px solid'}}>
                        {actions1}
                    </button>
                    <button onClick = {onAction2} style={{border: '1px solid'}}>
                        {actions2}
                    </button>
                </div>
            </div>   
         </div>
            
        )
    }
// }

// let buttons = document.querySelectorAll('.toggle-button');
// let modal = document.querySelector('#modal');
// [].forEach.call(buttons, function (button) {
//     button.addEventListener('click', function () {
//         modal.classList.toggle('off');
//     });
// });

Modal.propTypes = {
    actions1: PropTypes.string,
    actions2: PropTypes.string,
  };



 
