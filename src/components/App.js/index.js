import React, { useState, useEffect} from "react";
import Home from "../../pages/Home.js";
import Basketpage from "../../pages/Basketpage.js";
import Favoritepage from "../../pages/Favoritepage.js";
import { NavLink, Routes, Route } from "react-router-dom";
import Modal from '../Modal';
import Cards from '../Cards';
import Basket from '../Basket';
import iconstore from '../../image/iconstore80.png';
import Favorite from '../Favorite';
import { useFetch } from "../../hooks/use_fetch";
import './app.css';

export default function App() {


    const [openModal, setOpenModal] = useState (false);
    const [modal, setModal] = useState ({});
    const [cards, setCards] = useState ([]);
    const { data } = useFetch("card.json");
    const [basket, setBasket] = useState ([]);
    const [favoriteCards, setFavorite] = useState ([]);

    const closeModal = () => {
        setOpenModal(false)
    }

    const addToFavorites = (card) => {
        let newFavoriteCards = [...favoriteCards];
        if (favoriteCards.map(c => c.article).includes(card.article)) {
            console.log('remove');
            newFavoriteCards = favoriteCards.filter(c => c.article != card.article )
        } else {
            console.log('add');
            newFavoriteCards.push(card);
        }
        setFavorite(newFavoriteCards);
        console.log(newFavoriteCards.length);
        console.log(favoriteCards.length);
        localStorage.setItem('Favorite Toys', JSON.stringify(newFavoriteCards));
    }

    const deleteFavorites = (card ) => {
        console.log(card)
        let newFavoriteCards = [...favoriteCards];
        console.log(newFavoriteCards);
        if (favoriteCards.map(c => c.article).includes(card.article)) {
            console.log('removeFavorite');
            let idx = newFavoriteCards.findIndex(c => c.article == card.article);
            console.log(idx);
            newFavoriteCards.splice(idx, 1)
        }
        setFavorite(newFavoriteCards);
        localStorage.setItem('Favorite Toys', JSON.stringify(newFavoriteCards));
    }

    const addToBasket = (card) => {
        let newBasket = [...basket];
        newBasket.push(card);
        setBasket(newBasket);
        localStorage.setItem('Toys basket', JSON.stringify(newBasket));
    }    

    const deleteBasket = (card) => {
        let newBasket = [...basket];
        if (basket.map(c => c.article).includes(card.article)) {
            let idx = newBasket.findIndex(c => c.article == card.article);
            newBasket.splice(idx, 1)
        }
        setBasket(newBasket);
        localStorage.setItem('Toys basket', JSON.stringify(newBasket));
    }  

    const clickOpenModal = (modalId, card) => {
        let modal = modalsData[modalId];
        modal.header = "In the basket: " + card.name;
        modal.onAction1 = () => { 
            closeModal();
            addToBasket(card);
        };

        setModal(modal);
        setOpenModal(true);
    }

    const clickEditModal = (modalId, card) => {
        let modal = modalsData[modalId];
        modal.header = "Do you want remove " + card.name +"?";
        modal.onAction1 = () => { 
            closeModal();
            deleteBasket(card);
        };

        setModal(modal);
        setOpenModal(true);
    }



    const modalsData = {
        editBasket: {
            header: "Do you want to delete this file?",
            text: "Once you delete this file, it won't be possible to undo this action. Are you sure you want delete it?",
            actions1: "OK",
            actions2: "Cancel",
            onAction1: deleteBasket,
            onAction2: closeModal,
        }, 

        green: {
            header: "Do you want to save it?",
            text: "If not, it will be delete, choose an option:",
            actions1: "Save",
            actions2: "Delete"
        },

        addCard: {
            context: "Add to basket",
            text: "Are you sure you want add it?",
            actions1: "Confirm",
            actions2: "Cancel",
            //onAction1: addToBasket,
            onAction2: closeModal,
            // onAction3: this.addToFavorites,
        },

       
        }
    

    useEffect (() => {            
        setCards(data);
        let basketArrayRaw = localStorage.getItem (['Toys basket']) ;
        let basketArray  = basketArrayRaw ? JSON.parse(basketArrayRaw) : [];
        setBasket( basketArray );

        let favoriteArrayRaw = localStorage.getItem (['Favorite Toys']) ;
        let favoriteCards  = favoriteArrayRaw ? JSON.parse(favoriteArrayRaw) : [];
        setFavorite( favoriteCards );
    }, [data])


    return (
        <>  
            <div className='nav_wrapper'>
                <NavLink to="/">
                    <img src={iconstore} alt="Toys"></img>
                </NavLink>    
                <div className='counts_wrapper'>
                    <NavLink to="/favorite">
                        <Favorite favoriteCards = {favoriteCards}/>
                    </NavLink>
                    <NavLink to="/basket">
                        <Basket basket = {basket}/>
                    </NavLink>    
                </div>  
            </div>      
            <Routes>
                <Route path="/" element={
                                <div>  
                                <Cards cards = {cards} 
                                context = {'Add to basket'}
                                favoriteCards = {favoriteCards}
                                clickOpenModal = {clickOpenModal}
                                addToFavorites = {addToFavorites}
                                modalId = 'addCard'
                                /> 
                            </div>
                }/>
                <Route path="/basket" element={<Basketpage
                    context = {'Remove from basket'}
                    cards = {basket} 
                    favoriteCards = {favoriteCards}
                    clickOpenModal = {clickEditModal}
                    addToFavorites = {addToFavorites}
                    modalId = 'editBasket'
                    page="basket"
                />}/>
                <Route path="/favorite" element={<Favoritepage
                    context = {'Add to basket'}
                    cards = {favoriteCards} 
                    favoriteCards = {favoriteCards}
                    clickOpenModal = {clickOpenModal}
                    addToFavorites = {deleteFavorites}
                    modalId = 'addCard'
                    page="basket"
                />}/>
            </Routes>
            <Modal 
                modalsData = {modal}
                header = {modal.header}
                text = {modal.text}
                open = {openModal} 
                onClose = {closeModal} 
                onAction1 = {modal.onAction1}
                onAction2 = {modal.onAction2}
                actions1 = {modal.actions1} 
                actions2 = {modal.actions2}
             />

        </>
    )
}
