import React, { useState} from 'react';
import './card.css';
import Button from '../Button';
import starunfilled from '../../image/starunfilled.png';
import starfilled from '../../image/starfilled.png';
import PropTypes from 'prop-types';

export default function Card ({card, clickOpenModal, favorite, addToFavorites, modalId, context}) {

    
       const addToFavoritesClick  = () => {
         addToFavorites(card);
       }


        return(
            <div className='card'>
                <div style={{textAlign: 'center', marginTop: '15px'}}>
                    <img src={card.url} className='imgCard'></img>
                </div>
                <h1>{card.name}</h1>
                <h3>Price: {card.price}.00 euro</h3>
                <hr/>
                <p>Article: {card.article}</p>
                <p>Color: {card.color}</p>
                <div className='add_wrapper'>
                    <div className='button_wrapper'>
                        <Button text={context} onClick={clickOpenModal} modalId={modalId} card={card} backgroundColor='#bdbdbd'/>
                    </div>
                    <div>
                        <a onClick={addToFavoritesClick} >
                            <img src=  {!favorite ? starunfilled : starfilled } > 
                            </img> 
                        </a>
                    </div>
                </div>

            </div>
        )
    
}


Card.propTypes = {
    name: PropTypes.string,
    article: PropTypes.number,
    price: PropTypes.number,
    url: PropTypes.string,
    color: PropTypes.string
};

Card.defaultProps = {
    article: 0,
};



