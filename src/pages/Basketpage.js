import React from 'react';
import Cards from "../components/Cards";

export default function Basketpage ({cards, favoriteCards, clickOpenModal, addToFavorites, modalId, context}) {

    return (

        cards.length>0 ?  <Cards cards = {cards} 
        favoriteCards = {favoriteCards}
        clickOpenModal = {clickOpenModal}
        addToFavorites = {addToFavorites}
        modalId = {modalId}
        context= {context}
        // text='Edit to basket' 
        /> :  <h1> "В корзині товарів немає!"</h1>
       
    ) 
       
    
}
