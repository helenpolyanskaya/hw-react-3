import { useEffect, useState } from "react";

export const useFetch = (url) => {
  const [status, setStatus] = useState("");
  const [data, setData] = useState([]);
  useEffect(() => {
    setStatus("fetching");
    fetch(url)
      .then((res) => res.json())
      .then((fetchedData) => {
        setStatus("fetched");
        setData(fetchedData);
      });
  }, [url]);
  return { status, data };
};
